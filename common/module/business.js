// 用户身份相关接口

import http from '../http/interface'
const Set = {
	// 供应商
	get_suppliers: (data, ifLoad = false) => {
		return http.request({
			url: '/api/pda/suppliers/simple/index', 
			method: 'GET',
			data,
			// handle:true
		}, ifLoad)
	}, 
	// 仓库
	get_warehouse: (data, ifLoad = false) => {
		return http.request({
			url: '/api/pda/depots/simple/index', 
			method: 'GET',
			data,
			// handle:true
		}, ifLoad)
	}, 
	// 产品/*  */
	get_products: (data, ifLoad = false) => {
		return http.request({
			url: '/api/pda/products/tree/index', 
			method: 'GET',
			data,
			// handle:true
		}, ifLoad)
	}, 
	// 代理商
	get_merchant: (data, ifLoad = false) => {
		return http.request({
			url: '/api/pda/merchants/simple/index', 
			method: 'GET',
			data,
			// handle:true
		}, ifLoad)
	}, 
	
	// 入库校验
	get_checkCode: (data, ifLoad = false) => {
		return http.request({
			url: `/api/pda/stock/in/${data}/correct`, 
			method: 'GET',
			// data,
			// handle:true
		}, ifLoad)
	},  
	
	//入库
	post_ins: (data, ifLoad = false) => {
		return http.request({
			url: '/api/pda/stock/in', 
			method: 'POST',
			data,
			// handle:true
		}, ifLoad)
	}, 
	
	
	// 出库校验
	get_outCode: (data, ifLoad = false) => {
		return http.request({
			url: `/api/pda/stock/out/${data}/correct`, 
			method: 'GET',
			// data,
			// handle:true
		}, ifLoad)
	},  
	
	//出库
	post_out: (data, ifLoad = false) => {
		return http.request({
			url: '/api/pda/stock/out', 
			method: 'POST',
			data,
			// handle:true
		}, ifLoad)
	}, 
	
	
	// 出入库日志
	get_logs: (data, ifLoad = false) => {
		return http.request({
			url: '/api/pda/stock/logs', 
			method: 'GET',
			data,
			// handle:true
		}, ifLoad)
	}, 
	
	
	// 查询关联码
	get_codeDetail: (code, ifLoad = false) => {
		return http.request({
			url: `/api/pda/codes/${code}/info`, 
			method: 'GET',
			// data,
			// handle:true
		}, ifLoad)
	}, 
	
}
export default Set