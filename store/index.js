import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)
import http from '../common/http/index.js'
import SET from '@/SET.js';

// let rj_role_menus = uni.getStorageSync('rj_role_menus') 
// let rj_role_info = uni.getStorageSync('rj_role_info') 
const store = new Vuex.Store({
	state: {
		token: '',
		role:'',
		currentAction:'',
		hasLogin:false,
		inBunding:false,   //是否回调绑定页
		userInfo:{}, //用户得基本信息
		currentProType:'',   //当前的产品类型
		accountInfo:{}, //账户基本信息 通用
		currentOrder:'',  //当前订单信息
		roleMenus:{}  //权限列表
	},
	mutations: {
		// payload为用户传递的值，可以是单一值或者对象
		userLogin(state, payload){
			state.hasLogin = true;
			if(payload  &&payload.token){
				
				state.token = payload.token;
				uni.setStorageSync(SET.tokenName, 'Bearer '+   payload.token)
				// debugger
			}
			 
		},
		userLoginOut(state, payload){
			uni.removeStorageSync(SET.tokenName)
			// uni.removeStorageSync('rj_role_info')
			// uni.removeStorageSync('rj_role_menus') 
			// uni.removeStorageSync('inviAction')
			state.hasLogin = false;
			state.accountInfo = {};
			state.userInfo = {};
			uni.reLaunch({
				url:'/pages/login/index'
			})
		},
		setToken(state, payload) {
			// debugger
			state.token = payload;
		},
		setCurrentAction(state,payload){
			// debugger
			state.currentAction = {...payload};
		},
		inBunding(state,payload){
			state.inBunding = payload;
		},
		setUserInfo(state,payload){
			state.userInfo = {...payload};
		},
		setAccountInfo(state,payload){
			state.accountInfo = {...payload};
		},
		creatOrder(state,items){
			state.currentOrder = [...items]
			// debugger
		},
		setRoleMenus(state,items){
			state.roleMenus = {...items}
		},
		setCurrentProType(state,items){
			state.currentProType = items+''
		},
		setRoles(state,items){
			// alert(items)
			state.role = items+''
		}
	},
	actions:{
		async refreshUser({ state,commit }, payload){
			let  that  = this
			let ress =  await http.getConsumer();
			// console.log(res)
			if(ress.result===1){
				commit('userLogin')
				commit('setAccountInfo',ress.data)
				// alert(ress.data.consumer_type)
				let k = ress.data.consumer_type+''
				switch (ress.data.consumer_type){
					case '2': //员工
						commit('setRoles','员工')
						let res =  await http.crm_GetInfo();
						// console.log(res)
						if(res.result===1){
							let info = {...res.data.userInfo,name:res.data.name,is_admin:res.data.is_admin,avatar:res.data.avatar}	
							commit('setUserInfo',info)
							uni.setStorageSync('rj_role_info',menus)
							let menus = {}
							res.data.menus.map(item=>{
								menus[item.page_num] = item
							})
							console.log(menus)
							commit('setRoleMenus',menus)
							uni.setStorageSync('rj_role_menus',menus)
								
						}else{
							commit('userLoginOut')
							uni.showToast({
								title:'身份失效，重新登录'
							})
						}
						break;
					case '3'://会员
					commit('setRoles','会员')
						break;
					case '4'://业务员
					
					commit('setRoles','团长')
						break;
					case '5'://团长
					commit('setRoles','业务员')
						break;
					default:
						break;
				}

			}else{
				
			}
		},
		
		// async crmUserInfo({ state,commit }, payload){ 
		// 	alert(1)
			
		// },
	}
})

export default store