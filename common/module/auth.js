// 用户身份相关接口
// 我要积分
import http from '../http/interface'
const Auth = {
	
 
	// 登录
	userLogin: (data, ifLoad = false) => {
		return http.request({
			url: '/api/pda/login',
			method: 'POST',
			data,
		}, ifLoad)
	},
	
	// 获取用户信息
	 userInfo: (data, ifLoad = false) => {
	 	return http.request({
	 		url: '/api/pda/user/info',
	 		method: 'GET',
	 		data,
	 	}, ifLoad)
	 },
}
export default Auth
