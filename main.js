import Vue from 'vue'
import App from './App'
import store from './store/index.js';
import SET from '@/SET.js';
import api from './common/http/index.js'
 
Vue.config.productionTip = false

App.mpType = 'app'
import uView from '@/uni_modules/uview-ui'
Vue.use(uView)
Vue.prototype.$api = api

// 判断是否是微信浏览器
// #ifdef H5
let ua = navigator.userAgent.toLowerCase();
// let ifWx;
if (ua.match(/MicroMessenger/i) == "micromessenger") {
	uni.setStorageSync('ifWx', true)
} else {
	uni.setStorageSync('ifWx', false)
}
// #endif

 
const UI = {
	toast: function(text, duration, success) {
	
		// #ifdef H5 || MP-WEIXIN
		uni.showToast({
			title: text,
			icon: success ? 'success' : 'none',
			duration: duration || 1000
		})
		// #endif
		// #ifdef APP-PLUS
		plus.nativeUI.toast(text )
	 
		// #endif
	},
	showloading: function(text) {
		// #ifdef H5 || MP-WEIXIN
		uni.showLoading({
			title: text ? text : '加载中',
			mask: true
		})
		// #endif
		// #ifdef APP-PLUS
		plus.nativeUI.showWaiting(text = '请稍后...', {
			loading: {
				display: 'inline',
				// type:'snow'
			}
		})
		// #endif
	},
	hideloading: function() {
		// #ifdef H5 || MP-WEIXIN
		uni.hideLoading()
		// #endif
		// #ifdef APP-PLUS
		plus.nativeUI.closeWaiting()
		// #endif
	}
}

Vue.prototype.$ui = UI

const app = new Vue({
	store,
	...App
})
app.$mount()
