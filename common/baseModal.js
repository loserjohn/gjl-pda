// import http from '@/common/http/index.js';
import http from '@/common/http/interface'
import {
	mapState
} from 'vuex'
import SET from '@/SET.js'
module.exports = {
	data() {
		return {
			pageNum: 'PublicUser',
			baseUrl: '/Crm/PublicUser',
			hasRow: false,
			pageId: '',
			crmLists: [],
			crmTotal: 0,
			loadStatus: 'more',
			pageLoad: false,
			searchForm: {
				"pageIndex": 1, //页
				"pageSize": 10 //条
			},
			fabContent: [{
					text: '首页',
					iconPath: 'home',
					key: 'home',
					right: true
				},
				{
					text: '我的',
					iconPath: 'account',
					key: 'mine',
					right: true
				},
				{
					text: '顶部',
					iconPath: 'arrow-upward',
					key: 'backTop',
					right: true
				}
			],
			onBottom: false,
			onTop: false,
			lock: false,
			pageLoading: true,
			crmConfig: '', //当前页面的配置
			currenKeyType: '', //当前关键字类型
			currenKeyText: '', //当前关键字
			keyWord: '', //关键字
			unicode: '',
			timeVisible: false,
			currentTimePre: '',
			crmUploadUrl: SET.baseUrl + "/Upload/File/Saves",
			isAdmin:false,
			// rowingModal:false   //是不是划动使用上下拉刷新
		}
	},
	async onLoad() {
		// getRect挂载到$u上，因为这方法需要使用in(this)，所以无法把它独立成一个单独的文件导出
		
		console.log('页面',this.roleMenus)

		if (this.pageNum && this.roleMenus[this.pageNum]) {
			this.pageId = this.roleMenus[this.pageNum].page_id;
		}
		this.isAdmin = this.userInfo.is_admin == 1 ?true:false,
		// alert(this.isAdmin)
		this.unicode = this.userInfo.is_admin == 1 ? '' : this.userInfo.user_unit
		this.searchForm.user_unit = this.unicode


	},
	computed: {
		// 将vuex的state中的所有变量，解构到全局混入的mixin中
		...mapState(['hasLogin', 'userInfo', 'roleMenus'])
	},
	methods: {
		//创建唯一id
		_createValidId(){
			return new Date().getTime()
		},
			
		
		_showDetails(str) {
		  if (str) {
		    // console.log(str.replace('↵','\n'))
			str = str.replace(/\/br/g, "br\/")
			str = str.replace(/\n/g, "<br/>")
			// console.log(str)
		    return str
		  } else {
		    return '--'
		  }
		  
		},
		// 页面权限判断
		_roleCheck(pageNum){
			if(this.roleMenus && pageNum){
				return this.roleMenus[pageNum]?true:false
			}else{
				return false
			}
		},
		// 文字过滤
		_filterText(string, pro,str='-') {
			if (!this.crmConfig) return '-'
			let arr = this.crmConfig[pro];
			if (arr && arr.length) {
				let r = ''
				let s = arr.some(item => {
					if (string == item.value) {
						r = item.text
						return true
					} else {
						return false
					}
				})
				return s ? r : str
			} else {
				return str
			}
		},
		
		// 重置时间
		_clearTimePicker(prev) {
			this.searchForm[`${prev}_begin_time`] = '';
			this.searchForm[`${prev}_end_time`] = '';
			this.searchForm.pageIndex = 1
			this._getList()
		},
		_timePicker(prev) {
			this.currentTimePre = prev
			this.timeVisible = true
		},
		// 时间回调
		_changeTime(e) {
			let sta = e.startDate,
				end = e.endDate;
			this.searchForm[`${this.currentTimePre}_begin_time`] = sta;
			this.searchForm[`${this.currentTimePre}_end_time`] = end;
			this.searchForm.pageIndex = 1
			this._getList()
		},
		// 条件筛选
		_filterSearch(pro, val) {
			this.searchForm[pro] = val;
			this.searchForm.pageIndex = 1
			this._getList()
		},
		_serKeyWord(val, key) {
			this.currenKeyText = key;
			this.currenKeyType = val;
		},
		// 是否有权限做操作
		_rightCheck(action) {
			if (this.crmConfig.rights) {
				return this.crmConfig.rights.indexOf(action) >= 0 ? true : false
			} else {
				return true
			}
		},
		_pageChange(e) {
			// alert(e)
			this.searchForm.pageIndex = e
			this._getList()
		},

		// 接口入口获取页面配置
		async _getConfig() {
			this._configBefore()
			const res = await this.__getConfig()
			// console.log(res)
			if (res.result === 1) {
				this.crmConfig = res.data
			} else {
				uni.showToast({
					title: 'config获取失败'
				})
			}
			this._configAfter()
		},

		// 获取条目
		async _getList() {
			// console.log(this.searchForm)
			this._getListBefore()
			// uni.showLoading({})
			const res = await this.__getList(this.searchForm)
			// uni.hideLoading()

			if (res.result === 1) {
				this.crmLists = res.data.rows;
				this.crmTotal = res.data.total
			}
			this._getListAfter()
		},

		// 保存新建
		async _newSave(data) {
			this._saveNewBefore()
			uni.showLoading({})
			const res = await this.__insert(data)
			uni.hideLoading()
			if (res.result === 1) {
				this.$refs.uToast.show({
					title: '新建成功',
					type: 'success',
					position: 'bottom',
					back: true
				})
			} else {
				this.$refs.uToast.show({
					title: res.msg,
					type: 'error',
					position: 'center',
				})
			}
			this._saveNewAfter()
		},
		_formatImageStr(arr) {
			 
			let strs = JSON.stringify(arr)
			let res = strs.replace(/\"/g, '\'')
			return res
		},
		_formatStrImage(strs) {
			if(!strs)return [];
			let arrStr = strs.replace(/\'/g, '\"')
			return arrStr? JSON.parse(arrStr):[]
		},
		// 保存小批改
		async _updateSave(id, data) {
			this._saveUpdateBefore()
			uni.showLoading({})
			const res = await this.__update(id, data)
			uni.hideLoading()
			if (res.result === 1) {
				this.$refs.uToast.show({
					title: '保存成功',
					type: 'success',
					position: 'bottom',
					back: true

				})
			} else {
				this.$refs.uToast.show({
					title: res.msg,
					type: 'error',
					position: 'center',
				})
			}
			this._saveUpdateAfter()
		},
		// 获取详情
		async _getDetail(id) {
			this._getBefore()
			uni.showLoading({})
			const res = await this.__get(id)
			// console.log(res)
			uni.hideLoading()
			if (res.result === 1) {
				this.form = res.data
			}
			this._getAfter()
		},
		// 删除条目
		async _handleDelete(ids) {
			let that = this
			uni.showModal({
				title: '确认删除该条目？',
				async success(res) {
					if (res.confirm) {
						uni.showLoading({})
						const res = await that.__deletes(ids)
						uni.hideLoading()
						if (res.result === 1) {
							that.$refs.uToast.show({
								title: '删除成功',
								type: 'error',
								position: 'bottom',
							})
							that._getList()
						} else {
							that.$refs.uToast.show({
								title: res.msg,
								type: 'error',
								position: 'center',
							})
						}

					} else if (res.cancel) {
						console.log('用户点击取消');
					}
				}
			})


		},


		// 回调部分
		_configBefore() {},
		_configAfter() {},
		_getListBefore() {},
		_getListAfter() {},
		_getBefore() {},
		_getAfter() {},
		_saveNewBefore() {},
		_saveNewAfter() {},
		_saveUpdateBefore() {},
		_saveUpdateAfter() {},






		//api部分
		__deletes(ids) {
			return http.request({
				url: this.baseUrl + `/deletes/?ids=${ids}`,
				method: 'get'
			})
		},
		__getConfig(ifLoad = false) {
			return http.request({
				url: this.baseUrl + '/config?page_id=' + this.pageId,
				method: 'GET'
			}, ifLoad)
		},
		__getList(data, ifLoad = false) {
			return http.request({
				url: this.baseUrl + '/list',
				method: 'post',
				data
			}, ifLoad)
		},
		__insert(data, ifLoad = false) {
			return http.request({
				url: this.baseUrl + '/insert',
				method: 'post',
				data
			}, ifLoad)
		},
		__update(id, data) {
			return http.request({
				url: this.baseUrl + `/update/?id=${id}`,
				method: 'post',
				data
			})
		},
		__get(id) {
			return http.request({
				url: this.baseUrl + `/get/?id=${id}`,
				method: 'get'
			})
		},
		__getDetail(id) {
			return http.request({
				url: this.baseUrl + `/detail/?id=${id}`,
				method: 'get'
			})
		},

		_back() {
			uni.navigateBack({})
		},

	}
}
